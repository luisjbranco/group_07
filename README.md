# Group_07

A project dedicated to analyzing the energy mix of several countries, in order to contribute to a greener transition by having a more savvy task force.

# Group_07

A project dedicated to analyzing the energy mix of several countries, in order to contribute to a greener transition by having a more savvy task force.

## Project: Energy Mix 

## Project overview
We are a company participating in a two-day hackathon promoted to study the energy mix of several countries. We aim to contribute to the green transition by having a more savvy taskforce. The first day is dedicated for code heavy tasks and the second day is focused on polishing and finishing the project for presentation. In this project, using Pandas and Seaborn visualization we will explore the GDP and energy data to obtain an overview of country features.  

## Installation
This project requires Python and the following Python libraries installed: 

- Pandas 
- Matplotlib 
- Plotly-express 
- Seaborn 
- Requests 
- Os  

You will also need to have software installed to run and execute a Jupyter Notebook 
If you do not already have Python installed, it is highly suggested that you install the Anaconda distribution of Python, which includes all of the above items and more. 

## Code
The class and all the methods are provided in the ShutThePiano.py file and the data is in the [energy.csv](https://raw.githubusercontent.com/owid/energy-data/master/owid-energy-data.csv) file (already downloaded with the __init__ method) 

## Usage
There are 6 methods included in the project: 

1) get_data 
* This method, when called, downloads the energy-data csv file and saves it into a pandas data frame. 

2) get_countries 
* This method, when called, obtains the unique list of the available countries in the data set. 

3) area_chart 
* The method requests to input chosen country names and outputs an area chart of indexed energy consumption per year. This method contains country and normalize as arguments and needs to be called, for example, as area_chart("Portugal", normalize = True ). In this case, the country name needs to have the first letter capitalized and does not accept short forms, whereas normalize takes both True and False arguments (where with True the data will be normalized, and with False the opposite will occur). 

4) compare_country 
* To call the method you need to use compare_country( ) and then it will request to input a certain amount of countries to compare and then the name of the chosen countries. The output will be a line chart with the energy consumption per year per the selected countries.  

5) compare_gdp 
* This method will output the yearly GDP data for the chosen countries. To instantiate the function, you need to call the compare_gdp( ) method. It will then ask for the number of countries you want to compare, and after filling the gap it will display the exact number of countries chosen. The final output will be a line chart with the gdp for each country throughout the years. 

6) gapminder 
* The gapminder method takes the year and world arguments, and can be used like: gapminder(1992, world = False). This method will output a bubble chart with the energy consumption on the y axis and the gdp on the x axis. Moreover, it also shows the population size for each country as the size of the bubble. Saying so, the year argument has a range from 1970 to 2019 (depending on the countries chosen) and the world represent the aggregated values of all countries. In this case if we display world = False we will not have a bubble for the world, meaning that the rest of the countries will have a more amplified view (easier to analyze). 

## Authors and acknowledgment
Branco, Luís : 50409@novasbe.pt

Esteves, Filipa : 48842@novasbe.pt 

Mora, Isabel : 48516@novasbe.pt 

Paleckyte, Ieva : 48327@novasbe.pt 

## License
GNU GENERAL PUBLIC LICENSE

## Project status
Completed
