import requests
import csv
import pandas as pd
import os

class ShutThePiano:
    
    #def __init__(self):
    def download(self, url = 'https://raw.githubusercontent.com/owid/energy-data/master/owid-energy-data.csv'):
        """
        """
        
        self.url = url
        # self.name = name
        # self.dataframe_name = dataframe_name
        
        response = requests.get(url)  
        url_content = response.content
        csv_file = (r'..//downloads//energy.csv', 'wb')
        
        energy_df = pd.read_csv(r'..//downloads//energy.csv')
        